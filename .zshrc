# Path to your oh-my-zsh configuration.

# PATHS
export PATH=$PATH:~/.local/bin
export PATH=$PATH:/usr/lib/lightdm/lightdm:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games
export PATH=$PATH:~/local/bin
export PATH=$PATH:~/local/go/bin
export PATH=$PATH:node_modules/.bin

UNAME=`uname`
if [ "$UNAME" = "Darwin" ]; then
    export PATH=$PATH:/opt/local/bin
fi

export GOROOT=~/local/go

# zsh options
setopt inc_append_history

# variables
ZSH=~/.oh-my-zsh
WORKON_HOME=~/envs
IPYTHONDIR=~/.ipython

# oh-my-zsh configs
plugins=(git mercurial git-extras git-flow npm per-directory-history virtualenvwrapper)
ZSH_THEME="robbyrussell"
DISABLE_AUTO_UPDATE="true"
COMPLETION_WAITING_DOTS="true"

# exported variables
export VIRTUAL_ENV_DISABLE_PROMPT=1

# call external scripts
source $ZSH/oh-my-zsh.sh
source ~/.local/lib/python2.7/site-packages/powerline/bindings/zsh/powerline.zsh

#aliases
alias sdocker='sudo docker'


PATH=~/npm/bin/:$PATH:$HOME/.rvm/bin # Add RVM to PATH for scripting
NODE_PATH=$NODE_PATH:/home/oglu/npm/lib/node_modules
