#!/bin/sh

sudo apt-get install git, python-pip
echo "Getting rc from repository..."

git init
git remote add origin git@bitbucket.org:oglu/rc.git
git fetch origin
git checkout -b master --track origin/master 


echo "Installing fonts..."

if [ ! -d "~/.fonts" ]; then
    mkdir ~/.fonts
fi

if [ ! -d "~/fontconfig" ]; then
    mkdir -p ~/fontconfig/conf.d
fi

POWERLINE_GH=https://github.com/Lokaltog/powerline

wget --quiet $POWERLINE_GH/raw/develop/font/PowerlineSymbols.otf -O ~/.fonts/PowerlineSymbols.otf
wget --quiet $POWERLINE_GH/raw/develop/font/10-powerline-symbols.conf -O ~/fontconfig/conf.d/10-powerline-symbols.conf

fc-cache -vf

pip install --user git+git://github.com/Lokaltog/powerline


